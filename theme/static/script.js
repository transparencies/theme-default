'use strict';

const n = document.getElementById("next");
const next = n !== null ? n.href : window.location;
const p = document.getElementById("prev");
const prev = p !== null ? p.href : window.location;

document.addEventListener('keydown', (event) => {
	switch (event.key) {
		case "Enter":
		case " ":
			event.preventDefault();
		case "ArrowRight":
			window.location.assign(next);
			break;
		case "ArrowLeft":
			window.location.assign(prev);
			break;
	}
});

document.addEventListener("DOMContentLoaded", () => {
	renderMathInElement(document.body, {
		delimiters: [{
				left: '$$',
				right: '$$',
				display: true,
			},
			{
				left: '$',
				right: '$',
				display: false,
			},
		]
	});
});
